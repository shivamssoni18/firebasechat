package com.example.firebasechat.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.firebasechat.Activities.ChatActivity;
import com.example.firebasechat.ModelClass.LoginPojo;
import com.example.firebasechat.R;
import com.example.firebasechat.Utils.Temp_UserDetails;

import java.util.ArrayList;

public class RegisteredUserAdapter extends RecyclerView.Adapter<RegisteredUserAdapter.ViewHolder> {

    public RegisteredUserAdapter(ArrayList<LoginPojo> userLIST, Context context) {
        this.userLIST = userLIST;
        this.context = context;
    }

    private ArrayList<LoginPojo> userLIST;
    Context context;


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.chat_userlist_raw, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final LoginPojo user = userLIST.get(i);

        viewHolder.Uname.setText(user.getName());

        viewHolder.Uname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Temp_UserDetails.OtherUserId = user.getUserName();
                Intent i = new Intent(context, ChatActivity.class);

                i.putExtra("OtherUserid", user.getUserName());
                i.putExtra("OtherName", user.getName());

                context.startActivity(i);
//                Toast.makeText(context, "" + user.getUserName(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return userLIST.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView Uname;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            Uname = (TextView) itemView.findViewById(R.id.Uname);

        }
    }
}
